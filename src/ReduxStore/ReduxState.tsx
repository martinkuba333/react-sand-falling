import { createSlice } from '@reduxjs/toolkit';
import { ReduxStateInterface } from '../Interfaces/Interfaces';

const initialState: ReduxStateInterface = {
  value: 0,
  showGrid: 0,
};

const ReduxState = createSlice({
  name: 'sandGeneration',
  initialState,
  reducers: {
    incrementClickHover: (state) => {
      state.value += 1;
    },
    incrementShowGrid: (state) => {
      state.showGrid += 1;
    },
  },
});

export const { incrementClickHover, incrementShowGrid } = ReduxState.actions;
export default ReduxState.reducer;
