import styled from 'styled-components';

export const SidebarContainer = styled.div`
  position: absolute;
  background: #ddd;
  top: 0;
  bottom: 0;
  z-index: 5;
  transition: left 0.3s linear;
  box-shadow: inset 0px 0px 5px black;
`;

export const Hamburger = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 36px;
  height: 27px;
  cursor: pointer;
  border: 3px solid #333;
  padding: 5px 3px;
  border-radius: 9px;
  position: absolute;
  z-index: 2;
  top: 5px;
  left: calc(100% + 5px);
  box-shadow: inset 0px 0px 0px 500px white;
`;

export const Line = styled.div`
  width: 80%;
  height: 3px;
  background-color: #333;
  margin: auto;
  transition: transform 0.3s ease;
`;

export const X = styled.div`
  text-align: center;
  font-weight: bold;
  font-size: 33px;
  position: absolute;
  top: -2px;
  left: 10px;
  pointer-events: none;
`;
