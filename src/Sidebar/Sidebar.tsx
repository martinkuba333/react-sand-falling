import React, { useEffect, useState } from 'react';
import { Formular } from '../Formular/Formular';
import { SidebarProps } from '../Interfaces/Interfaces';
import { SidebarContainer, Hamburger, Line, X } from './Sidebar.styles';
import { Title } from '../Utils/Global.styles';

const widthByWindowSize: string = window.innerWidth > 700 ? '20%' : '50%';

export const Sidebar: React.FC<SidebarProps> = (props: SidebarProps) => {
  const [isHidden, setIsHidden] = useState<boolean>(true);

  useEffect(() => {
    setIsHidden(props.hideSidebarOnLoad);
  }, [props.hideSidebarOnLoad]);

  const createFormular = (): JSX.Element => {
    return (
      <>
        <Formular
          processSubmit={props.processSubmit}
          processReset={props.processReset}
          defaultSubmitStates={props.defaultSubmitStates}
        />
      </>
    );
  };

  const switchSidebarVisibility = () => {
    setIsHidden(!isHidden);
  };

  const createHamburgerMenu = (): JSX.Element => {
    const titleHover = 'Open/Close menu';

    return (
      <>
        <Hamburger onClick={switchSidebarVisibility} title={titleHover}>
          {isHidden ? (
            <>
              <Line />
              <Line />
              <Line />
            </>
          ) : (
            <X>X</X>
          )}
        </Hamburger>
      </>
    );
  };

  return (
    <>
      <SidebarContainer
        style={
          isHidden
            ? {
                left: `calc(-${widthByWindowSize})`,
                width: widthByWindowSize,
              }
            : {
                left: 0,
                width: widthByWindowSize,
              }
        }
      >
        {createHamburgerMenu()}
        <Title>Configuration</Title>
        {createFormular()}
      </SidebarContainer>
    </>
  );
};
