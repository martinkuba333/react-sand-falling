import React, { useEffect, useState } from 'react';
import { CONST } from '../Utils/Constants';
import { Square } from '../Square/Square';
import { SquareValuesArrayInitEffect } from '../Hooks';
import { generateRandomArray, getRandomSandHexColor } from '../Helper/calculations';
import { SquareArrayValue, SquaresProps, SquareValue } from '../Interfaces/Interfaces';
import { useSelector } from 'react-redux';
import { SquaresContainer } from './Squares.styles';

const MOUSE_ACTION_ENTER = 'enter';

export const Squares: React.FC<SquaresProps> = (props: SquaresProps) => {
  const [squaresValues, setSquaresValues] = useState<SquareValue[]>([]);
  const [squares, setSquares] = useState<JSX.Element[]>([]);
  const [gravityStarted, setGravityStarted] = useState<boolean>(false);
  const [randomHoverColor, setRandomHoverColor] = useState<string>(getRandomSandHexColor());
  const [randomClickColor, setRandomClickColor] = useState<string>(getRandomSandHexColor());
  const reduxSandGenerationCounter = useSelector((state: any) => state.counter.value);
  const redux_show_grid = useSelector((state: any) => state.counter.showGrid);

  SquareValuesArrayInitEffect(setSquaresValues, props.submitValues);

  useEffect(() => {
    const squareArray: SquareArrayValue[] = [];

    for (let i = 0; i < squaresValues.length; i++) {
      const { index, isSand, color } = squaresValues[i];
      squareArray.push({
        index,
        isSand,
        color,
      });
    }

    setSquares(
      squareArray.map((squareValue: SquareArrayValue) => (
        <Square
          key={squareValue.index}
          index={squareValue.index}
          isSand={squareValue.isSand}
          processSquareClick={processSquareClick}
          color={squareValue.color}
          submitValues={props.submitValues}
          randomHoverColor={randomHoverColor}
          randomClickColor={randomClickColor}
          reduxSandGenerationCounter={reduxSandGenerationCounter % 2 === 0}
          redux_show_grid={redux_show_grid}
        />
      )),
    );
  }, [squaresValues]);

  useEffect(() => {
    let intervalId: NodeJS.Timeout;
    if (gravityStarted) {
      intervalId = setInterval(() => {
        moveEverySquareDown();
        enableFalling();
      }, props.submitValues.fallInterval);
    }
    return () => clearInterval(intervalId);
  }, [gravityStarted, props.submitValues.fallInterval]);

  const moveEverySquareDown = (): void => {
    const newSquareValues = [];

    for (let i = squaresValues.length - 1; i >= 0; i--) {
      if (
        squaresValues[i].isSand &&
        i + props.submitValues.lineCount <
          props.submitValues.lineCount * props.submitValues.columnCount &&
        !squaresValues[i + props.submitValues.lineCount].isSand &&
        !squaresValues[i].stopFalling
      ) {
        squaresValues[i].isSand = false;
        squaresValues[i + props.submitValues.lineCount].isSand = true;
        squaresValues[i + props.submitValues.lineCount].stopFalling = true;
        squaresValues[i + props.submitValues.lineCount].color = squaresValues[i].color;
        squaresValues[i].color = CONST.BASE_SQUARE_COLOR;
      }

      if (
        squaresValues[i].isSand &&
        i + props.submitValues.lineCount - 1 <
          props.submitValues.lineCount * props.submitValues.columnCount &&
        !squaresValues[i + props.submitValues.lineCount - 1].isSand &&
        !squaresValues[i].stopFalling &&
        i % props.submitValues.lineCount !== 0
      ) {
        squaresValues[i].isSand = false;
        squaresValues[i + props.submitValues.lineCount - 1].isSand = true;
        squaresValues[i + props.submitValues.lineCount - 1].stopFalling = true;
        squaresValues[i + props.submitValues.lineCount - 1].color = squaresValues[i].color;
        squaresValues[i].color = CONST.BASE_SQUARE_COLOR;
      }

      if (
        squaresValues[i].isSand &&
        i + props.submitValues.lineCount + 1 <
          props.submitValues.lineCount * props.submitValues.columnCount &&
        !squaresValues[i + props.submitValues.lineCount + 1].isSand &&
        !squaresValues[i].stopFalling &&
        (i + props.submitValues.lineCount + 1) % props.submitValues.lineCount !== 0
      ) {
        squaresValues[i].isSand = false;
        squaresValues[i + props.submitValues.lineCount + 1].isSand = true;
        squaresValues[i + props.submitValues.lineCount + 1].stopFalling = true;
        squaresValues[i + props.submitValues.lineCount + 1].color = squaresValues[i].color;
        squaresValues[i].color = CONST.BASE_SQUARE_COLOR;
      }

      newSquareValues.push(squaresValues[i]);
    }
    setSquaresValues(newSquareValues);
  };

  const enableFalling = (): void => {
    const newSquareValues = [];
    for (let i = 0; i < squaresValues.length; i++) {
      squaresValues[i].stopFalling = false;
      newSquareValues.push(squaresValues[i]);
    }
    setSquaresValues(newSquareValues);
  };

  const processSquareClick = (index: number, color: string) => {
    setGravityStarted(true);
    setRandomClickColor(getRandomSandHexColor);

    const totalSquares = props.submitValues.lineCount * props.submitValues.columnCount;

    const randomArray: number[] = generateRandomArray();

    setSquaresValues((prevSquares: SquareValue[]) => {
      const updatedSquares = [...prevSquares];

      if (!updatedSquares[index].isSand) {
        updatedSquares[index].isSand = true;
        updatedSquares[index].color = color;
      }

      updateAdjacentSquares(updatedSquares, index, totalSquares, color, randomArray);

      return updatedSquares;
    });
  };

  const updateAdjacentSquares = (
    updatedSquares: SquareValue[],
    index: number,
    totalSquares: number,
    color: string,
    randomArray: number[],
  ): void => {
    const isValidSquare = (squareIndex: number) =>
      squareIndex >= 0 && squareIndex < totalSquares && !updatedSquares[squareIndex].isSand;

    const row = Math.floor(index / props.submitValues.lineCount);
    const col = index % props.submitValues.columnCount;
    let counter = 0;

    for (let i = row - 1; i <= row + 1; i++) {
      for (let j = col - 1; j <= col + 1; j++) {
        if (
          i >= 0 &&
          j >= 0 &&
          i < props.submitValues.lineCount &&
          j < props.submitValues.columnCount &&
          (i !== row || j !== col) &&
          randomArray.indexOf(counter) > -1
        ) {
          const adjacentIndex = i * props.submitValues.lineCount + j;
          if (isValidSquare(adjacentIndex)) {
            updatedSquares[adjacentIndex].isSand = true;
            updatedSquares[adjacentIndex].color = color;
          }
        }
        counter++;
      }
    }
  };

  const mouseEffect = (mouseAction: string) => {
    if (mouseAction === MOUSE_ACTION_ENTER) {
      setRandomHoverColor(getRandomSandHexColor);
    }
  };

  return (
    <>
      <SquaresContainer
        onMouseEnter={() => {
          mouseEffect(MOUSE_ACTION_ENTER);
        }}
        onClick={() => {
          setRandomHoverColor(getRandomSandHexColor);
        }}
      >
        {squares}
      </SquaresContainer>
    </>
  );
};
