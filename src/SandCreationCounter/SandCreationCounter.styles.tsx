import styled from 'styled-components';
import { submitButtonStyles } from '../Utils/Global.styles';

const sandCreationColor = 'black';
export const SandCreation = styled.div`
  border: 2px solid ${sandCreationColor};
  background: white;
  color: ${sandCreationColor};
  ${submitButtonStyles};
  display: block;
  transition: background 0.15s ease-in;

  &:hover {
    cursor: pointer;
    background: ${sandCreationColor};
    color: white;
  }
`;
