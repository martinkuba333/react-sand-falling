import React, { ReactElement, useState } from 'react';
import { useDispatch } from 'react-redux';
import { incrementClickHover } from '../ReduxStore/ReduxState';
import { SandCreation } from './SandCreationCounter.styles';

const CLICK_TEXT = 'Sand on click';
const HOVER_TEXT = 'Sand on hover';

export function SandCreationCounter(): ReactElement {
  const [sandGenerationText, setSandGenerationText] = useState<string>(CLICK_TEXT);
  const dispatch = useDispatch();

  const processSandCreationClick = () => {
    setSandGenerationText(sandGenerationText === CLICK_TEXT ? HOVER_TEXT : CLICK_TEXT);
    dispatch(incrementClickHover());
  };

  return (
    <>
      <SandCreation onClick={processSandCreationClick}>{sandGenerationText}</SandCreation>
    </>
  );
}
