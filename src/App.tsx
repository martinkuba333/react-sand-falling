import React, { ReactElement, useEffect, useState } from 'react';
import { Board, Text } from './App.styles';
import { Squares } from './Squares/Squares';
import { CONST } from './Utils/Constants';
import { SubmitStates } from './Interfaces/Interfaces';
import { Sidebar } from './Sidebar/Sidebar';
import { Title } from './Utils/Global.styles';

const WINDOW_SMALL_WIDTH = 900;
const WINDOW_VERY_SMALL_WIDTH = 600;

const defaultSubmitStates: SubmitStates = {
  lineCount: CONST.LINE_COUNT,
  columnCount: CONST.COLUMN_COUNT,
  fallInterval: CONST.FALL_INTERVAL,
  squareWidth:
    window.innerWidth < WINDOW_VERY_SMALL_WIDTH ? CONST.SQUARE_SMALL_WIDTH : CONST.SQUARE_WIDTH,
  squareHeight:
    window.innerWidth < WINDOW_VERY_SMALL_WIDTH ? CONST.SQUARE_SMALL_HEIGHT : CONST.SQUARE_HEIGHT,
};

function App(): ReactElement<any, any> {
  const [submitValues, setSubmitValues] = useState<SubmitStates>(defaultSubmitStates);
  const [hideSidebarOnLoad, setHideSidebarOnLoad] = useState<boolean>(true);

  useEffect(() => {
    setHideSidebarOnLoad(window.innerWidth < WINDOW_SMALL_WIDTH);
  }, []);

  const processSubmit = (values: SubmitStates) => {
    setSubmitValues(values);
  };

  const processReset = (values: SubmitStates) => {
    setSubmitValues(values);
  };

  const createTexts = (): JSX.Element => {
    return (
      <>
        <Title>FALLING SAND SIMULATION</Title>
        <Text>Click or hover on board to make some sand!</Text>
      </>
    );
  };

  return (
    <>
      {createTexts()}
      <Sidebar
        processSubmit={processSubmit}
        processReset={processReset}
        defaultSubmitStates={submitValues}
        hideSidebarOnLoad={hideSidebarOnLoad}
      />
      <Board>
        <Squares submitValues={submitValues} />
      </Board>
    </>
  );
}

export default App;
