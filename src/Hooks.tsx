import React, { Dispatch, SetStateAction, useEffect } from 'react';
import { CONST } from './Utils/Constants';
import { SquareValue, SubmitStates } from './Interfaces/Interfaces';

export const SquareValuesArrayInitEffect = (
  setSquaresValues: Dispatch<SetStateAction<SquareValue[]>>,
  submitValues: SubmitStates,
): void => {
  useEffect((): void => {
    let squareValuesArray = [];
    for (let i = 0; i < submitValues.lineCount; i++) {
      for (let j = 0; j < submitValues.columnCount; j++) {
        const key = i * submitValues.columnCount + j;
        squareValuesArray.push({
          index: key,
          x: i,
          y: j,
          isSand: false,
          stopFalling: false,
          color: CONST.BASE_SQUARE_COLOR,
        });
      }
    }

    setSquaresValues(squareValuesArray);
  }, []);
};
