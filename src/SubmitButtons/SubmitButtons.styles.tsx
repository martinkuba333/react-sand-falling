import styled from 'styled-components';
import { submitButtonStyles } from '../Utils/Global.styles';

export const SubmitButtonsContainer = styled.div`
  width: fit-content;
  display: block;
  margin: auto;
`;

const submitColor = 'green';
export const Submit = styled.div`
  border: 2px solid ${submitColor};
  background: white;
  color: ${submitColor};
  transition: background 0.15s ease-in;

  ${submitButtonStyles}
  &:hover {
    cursor: pointer;
    background: ${submitColor};
    color: white;
  }
`;

const resetColor = '#be0909';
export const Reset = styled.div`
  border: 2px solid ${resetColor};
  background: white;
  color: ${resetColor};
  transition: background 0.15s ease-in;

  ${submitButtonStyles}
  &:hover {
    cursor: pointer;
    background: ${resetColor};
    color: white;
  }
`;

const refreshColor = 'blue';
export const Refresh = styled.div`
  border: 2px solid ${refreshColor};
  background: white;
  color: ${refreshColor};
  transition: background 0.15s ease-in;

  ${submitButtonStyles}
  &:hover {
    cursor: pointer;
    background: ${refreshColor};
    color: white;
  }
`;
