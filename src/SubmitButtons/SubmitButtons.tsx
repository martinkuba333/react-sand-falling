import React from 'react';
import { SubmitButtonsContainer, Submit, Reset, Refresh } from './SubmitButtons.styles';
import { SandCreationCounter } from '../SandCreationCounter/SandCreationCounter';
import { ShowGrid } from '../ShowGrid/ShowGrid';
import { SubmitButtonsProps } from '../Interfaces/Interfaces';

const SUBMIT_TEXT = 'Submit values';
const RESET_TEXT = 'Reset values';
const REFRESH_TEXT = 'Refresh page';

export const SubmitButtons: React.FC<SubmitButtonsProps> = (props: SubmitButtonsProps) => {
  const processSubmitClick = () => {
    props.processSubmitClick();
  };

  const processResetClick = () => {
    props.processResetClick();
  };

  const processRefreshClick = () => {
    window.location.reload();
  };

  return (
    <SubmitButtonsContainer>
      <Submit onClick={processSubmitClick}>{SUBMIT_TEXT}</Submit>
      <Reset onClick={processResetClick}>{RESET_TEXT}</Reset>
      <ShowGrid></ShowGrid>
      <SandCreationCounter></SandCreationCounter>
      <Refresh onClick={processRefreshClick}>{REFRESH_TEXT}</Refresh>
    </SubmitButtonsContainer>
  );
};
