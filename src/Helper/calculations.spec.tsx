import { getRandomSandHexColor, generateRandomArray, SAND_COLORS } from './calculations';

describe('getRandomSandHexColor', () => {
  test('returns a valid hexadecimal color from SAND_COLORS', () => {
    const color = getRandomSandHexColor();
    const isValidColor = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/.test(color); // Regular expression to check if it's a valid hexadecimal color
    expect(isValidColor).toBe(true);
  });

  test('returns a color from SAND_COLORS array', () => {
    const color = getRandomSandHexColor();
    const isInSandColors = SAND_COLORS.includes(color);
    expect(isInSandColors).toBe(true);
  });
});

describe('generateRandomArray', () => {
  test('returns an array of length 5', () => {
    const array = generateRandomArray();
    expect(array).toHaveLength(5);
  });

  test('returns an array containing numbers from 0 to 8', () => {
    const array = generateRandomArray();
    const isValidNumbers = array.every((num) => num >= 0 && num <= 8);
    expect(isValidNumbers).toBe(true);
  });

  test('returns an array with unique numbers', () => {
    const array = generateRandomArray();
    const uniqueArray = Array.from(new Set(array)); // Remove duplicates
    expect(uniqueArray).toHaveLength(array.length);
  });

  test('returns different arrays on multiple calls', () => {
    const array1 = generateRandomArray();
    const array2 = generateRandomArray();
    expect(array1).not.toEqual(array2);
  });
});
