export const SAND_COLORS = ['#e9d9c7', '#d6ba98', '#c49b69', '#b58447', '#866234', '#5d4424'];

export const getRandomSandHexColor = (): string => {
  const randomIndex = Math.floor(Math.random() * SAND_COLORS.length);
  return SAND_COLORS[randomIndex];
};

function shuffleArray(array: number[]): number[] {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

export function generateRandomArray(): number[] {
  const values = [0, 1, 2, 3, 4, 5, 6, 7, 8];
  const shuffledValues = shuffleArray(values);
  return shuffledValues.slice(0, 5);
}
