import styled from 'styled-components';

export const FormGroupContainer = styled.div`
  margin: 5px 5px 0;
  background: #efefef;
  display: block;
  text-align: center;
`;

export const Label = styled.label`
  margin-top: 8px;
  padding: 2px 5px;
  text-align: center;
  font-weight: bold;
  display: inline-block;
`;

export const Input = styled.input`
  padding: 8px;
  border: 1px solid #ccc;
  border-radius: 4px;
  width: 40px;
  margin: auto;
  text-align: center;
  font-size: 20px;
  font-weight: bold;
  float: right;
`;
