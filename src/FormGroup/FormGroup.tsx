import React from 'react';
import { FormGroupContainer, Label, Input } from './FormGroup.styles';
import { FormGroupProps } from '../Interfaces/Interfaces';

export const FormGroup: React.FC<FormGroupProps> = (props: FormGroupProps) => {
  const createFormGroup = (): JSX.Element => {
    const { dataState, dataSetState, dataLabel, processSubmitClick } = props;
    const enterKey = 'Enter';
    return (
      <FormGroupContainer>
        <Label>{dataLabel}</Label>
        <Input
          type={'text'}
          value={dataState.toString()}
          onKeyPress={(e) => {
            if (e.code === enterKey) {
              processSubmitClick();
            }
          }}
          onChange={(e) => {
            dataSetState(e.target.value);
          }}
        />
      </FormGroupContainer>
    );
  };

  return <>{createFormGroup()}</>;
};
