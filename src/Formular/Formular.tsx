import React, { useEffect, useState } from 'react';
import { FormContainer } from './Formular.styles';
import { CONST } from '../Utils/Constants';
import { SubmitButtons } from '../SubmitButtons/SubmitButtons';
import { FormGroup } from '../FormGroup/FormGroup';
import { FormularProps } from '../Interfaces/Interfaces';

export const Formular: React.FC<FormularProps> = (props: FormularProps) => {
  const [lineCount, setLineCount] = useState<number>(props.defaultSubmitStates.lineCount);
  const [columnCount, setColumnCount] = useState<number>(props.defaultSubmitStates.columnCount);
  const [fallInterval, setFallInterval] = useState<number>(props.defaultSubmitStates.fallInterval);
  const [squareWidth, setSquareWidth] = useState<number>(props.defaultSubmitStates.squareWidth);
  const [squareHeight, setSquareHeight] = useState<number>(props.defaultSubmitStates.squareHeight);

  const formGroupData = [
    // { state: lineCount, setState: setLineCount, label: 'Squares in line' },
    // { state: columnCount, setState: setColumnCount, label: 'Squares in column' },
    { state: fallInterval, setState: setFallInterval, label: 'Fall speed' },
    { state: squareWidth, setState: setSquareWidth, label: 'Square width' },
    { state: squareHeight, setState: setSquareHeight, label: 'Square height' },
  ];

  useEffect(() => {
    setLineCount(props.defaultSubmitStates.lineCount);
    setColumnCount(props.defaultSubmitStates.columnCount);
    setFallInterval(props.defaultSubmitStates.fallInterval);
    setSquareWidth(props.defaultSubmitStates.squareWidth);
    setSquareHeight(props.defaultSubmitStates.squareHeight);
  }, [props]);

  const processSubmitClick = () => {
    const validateAndClamp = (
      value: number,
      min: number,
      max: number,
      defaultValue: number,
    ): number => {
      if (isNaN(value) || value < min || value > max) {
        return defaultValue;
      }
      return value;
    };
    console.log(CONST.LINE_COUNT);
    const newLineCount = validateAndClamp(lineCount, 10, 500, CONST.LINE_COUNT);
    const newFallInterval = validateAndClamp(fallInterval, 5, Infinity, CONST.FALL_INTERVAL);
    const newSquareWidth = validateAndClamp(squareWidth, 1, Infinity, CONST.SQUARE_WIDTH);
    const newSquareHeight = validateAndClamp(squareHeight, 1, Infinity, CONST.SQUARE_HEIGHT);

    props.processSubmit({
      lineCount: newLineCount,
      columnCount: newLineCount, //I want proper square
      fallInterval: newFallInterval,
      squareWidth: newSquareWidth,
      squareHeight: newSquareHeight,
    });
  };

  const processResetClick = () => {
    props.processReset({
      lineCount: CONST.LINE_COUNT,
      columnCount: CONST.COLUMN_COUNT,
      fallInterval: CONST.FALL_INTERVAL,
      squareWidth: CONST.SQUARE_WIDTH,
      squareHeight: CONST.SQUARE_HEIGHT,
    });
  };

  const createContainter = (): JSX.Element => {
    return (
      <FormContainer>
        {formGroupData.map((data: any, index: number) => (
          <React.Fragment key={index}>
            <FormGroup
              dataState={data.state}
              dataSetState={data.setState}
              dataLabel={data.label}
              processSubmitClick={processSubmitClick}
            />
          </React.Fragment>
        ))}
      </FormContainer>
    );
  };

  return (
    <>
      {createContainter()}
      <SubmitButtons
        processSubmitClick={processSubmitClick}
        processResetClick={processResetClick}
      />
    </>
  );
};
