import React from 'react';
import styled from 'styled-components';

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 100%;
  width: fit-content;
  margin: auto;
  padding-top: 15px;
  clear: both;
`;
