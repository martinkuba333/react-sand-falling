import styled from 'styled-components';
import { CONST } from '../Utils/Constants';

interface BoxProps {
  index: number;
  width: number;
  height: number;
}

export const Box = styled.div<BoxProps>`
  width: ${(props) => (props.width ? props.width : CONST.SQUARE_WIDTH)}px;
  height: ${(props) => (props.height ? props.height : CONST.SQUARE_HEIGHT)}px;
  float: left;
  clear: ${(props) => (props.index % CONST.LINE_COUNT === 0 ? 'both' : 'none')};
  user-select: none;

  &:hover {
    cursor: pointer;
  }
`;
