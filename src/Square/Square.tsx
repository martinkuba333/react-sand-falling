import React from 'react';
import { Box } from './Square.styles';
import { SquareProps } from '../Interfaces/Interfaces';

export const Square: React.FC<SquareProps> = (props: SquareProps) => {
  const squareClick = () => {
    if (props.reduxSandGenerationCounter) {
      props.processSquareClick(props.index, props.randomClickColor);
    }
  };

  const mouseEnterEffect = () => {
    if (!props.reduxSandGenerationCounter) {
      props.processSquareClick(props.index, props.randomHoverColor);
    }
  };

  return (
    <>
      <Box
        index={props.index}
        style={{
          background: props.color,
          boxShadow: props.redux_show_grid % 2 !== 0 ? 'inset 0px 0px 0px 1px #c5afaf69' : 'none',
        }}
        width={props.submitValues.squareWidth}
        height={props.submitValues.squareHeight}
        onClick={squareClick}
        onMouseEnter={() => {
          mouseEnterEffect();
        }}
        onTouchStart={() => {
          mouseEnterEffect();
        }}
        onTouchMove={() => {
          mouseEnterEffect();
        }}
      ></Box>
    </>
  );
};
