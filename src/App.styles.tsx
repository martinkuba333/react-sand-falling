import styled from 'styled-components';

export const Board = styled.div`
  margin: 15px auto;
  width: fit-content;
  display: block;
`;

export const Text = styled.p`
  text-align: center;
  margin: 2px auto;
`;
