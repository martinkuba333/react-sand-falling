import React from 'react';

export interface SubmitStates {
  lineCount: number;
  columnCount: number;
  fallInterval: number;
  squareWidth: number;
  squareHeight: number;
}

export interface FormGroupProps {
  dataState: number;
  dataSetState: React.Dispatch<React.SetStateAction<number | string>>;
  dataLabel: string;
  processSubmitClick: () => void;
}

export interface FormularProps {
  processSubmit: (values: SubmitStates) => void;
  processReset: (values: SubmitStates) => void;
  defaultSubmitStates: SubmitStates;
}

export interface SidebarProps extends FormularProps {
  hideSidebarOnLoad: boolean;
}

export interface SquareProps {
  key: number;
  index: number;
  isSand: boolean;
  processSquareClick: (index: number, color: string) => void;
  color: string;
  submitValues: SubmitStates;
  randomHoverColor: string;
  randomClickColor: string;
  reduxSandGenerationCounter: boolean;
  redux_show_grid: number;
}

export interface SquareValue {
  index: number;
  x: number;
  y: number;
  isSand: boolean;
  stopFalling: boolean;
  color: string;
}

export interface SquaresProps {
  submitValues: SubmitStates;
}

export interface SquareArrayValue {
  index: number;
  isSand: boolean;
  color: string;
}

export interface SubmitButtonsProps {
  processSubmitClick: () => void;
  processResetClick: () => void;
}

export interface ReduxStateInterface {
  value: number;
  showGrid: number;
}
