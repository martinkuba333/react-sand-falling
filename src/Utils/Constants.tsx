export const CONST = {
  LINE_COUNT: 80,
  COLUMN_COUNT: 80,
  SQUARE_WIDTH: 8,
  SQUARE_SMALL_WIDTH: 4,
  SQUARE_HEIGHT: 8,
  SQUARE_SMALL_HEIGHT: 4,
  FALL_INTERVAL: 50,
  BASE_SQUARE_COLOR: '#eee',
};

//TODO IDEAS
// all values from here configurable
// refactor
// more effectve, lags are presented - use memo?
// make big circle around mouse
// create cellar or other obstacles
// faster falling after a while?
// stop moving button
// different square count on line/column doesn't work
