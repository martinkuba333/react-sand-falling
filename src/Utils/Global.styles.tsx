import styled from 'styled-components';

export const submitButtonStyles = `
  width: fit-content;
  height: 20px;
  padding: 5px;
  text-transform: uppercase;
  font-weight: bold;
  margin: 20px 10px 0px;
  display: inline-block;
  border-radius: 3px;
  user-select: none;
  width: 80%;
  text-align: center; 
`;

export const Title = styled.h3`
  text-align: center;
  margin-top: 10px;
  margin-bottom: 5px;
`;
