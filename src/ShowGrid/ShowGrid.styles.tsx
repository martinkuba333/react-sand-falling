import styled from 'styled-components';
import { submitButtonStyles } from '../Utils/Global.styles';

const showGridColor = 'grey';
export const ShowGridButton = styled.div`
  ${submitButtonStyles};
  border: 2px solid ${showGridColor};
  background: white;
  color: ${showGridColor};
  transition: background 0.15s ease-in;

  &:hover {
    cursor: pointer;
    background: ${showGridColor};
    color: white;
  }
`;
