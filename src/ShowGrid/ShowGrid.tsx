import React, { useState } from 'react';
import { ShowGridButton } from './ShowGrid.styles';
import { useDispatch } from 'react-redux';
import { incrementShowGrid } from '../ReduxStore/ReduxState';

const SHOW_GRID_TEXT = 'Show grid';
const HIDE_GRID_TEXT = 'Hide grid';

export function ShowGrid(): JSX.Element {
  const [showGridText, setShowGridText] = useState<string>(SHOW_GRID_TEXT);
  const dispatch = useDispatch();

  const processShowGridClick = () => {
    setShowGridText(showGridText === SHOW_GRID_TEXT ? HIDE_GRID_TEXT : SHOW_GRID_TEXT);
    dispatch(incrementShowGrid());
  };

  return (
    <>
      <ShowGridButton onClick={processShowGridClick}>{showGridText}</ShowGridButton>
    </>
  );
}
